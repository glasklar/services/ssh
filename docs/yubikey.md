# SSH with a YubiKey

This document provides some pointers on how to SSH with a YubiKey.

## `ed25519-sk`

SSH >=v8.2 added [U2F/FIDO support][].  A gist is that it is possible to have an
Ed25519 key-pair that is backed by a unique device secret stored on a YubiKey.

[U2F/FIDO support]: https://www.openssh.com/txt/release-8.2

Check SSH version:

    $ ssh -V
    OpenSSH_9.0p1 Debian-1+b1, OpenSSL 3.0.5 5 Jul 2022

Plug in a YubiKey, then generate a key-pair using the _signing key_ mode:

    ssh-keygen -t ed25519-sk -C "$(USER).yubikey"

The above will write two files to disk:

  - `~/.ssh/id_ed25519_sk`
  - `~/.ssh/id_ed25519_sk.pub`

The first file contains a ["key handle"][].  It is used to derive an Ed25519
private key from a unique device secret.  The key handle is really just a nonce
and a MAC, and the (re)derived private key never leaves the card on usage.

["key handle"]: https://www.yubico.com/blog/yubicos-u2f-key-wrapping/

The second file contains the corresponding public key.

Append `id_ed25519_sk.pub` to the `authorized_keys` file to start SSH:ing.  Make
sure that there is a reasonable restore-path in case a YubiKey is lost.

**Warning:**
`ssh-keygen -Y sign` uses a [different format][] for `*-sk` key-pairs, i.e., not
to be confused with the [normal signing format][].  If you are looking to sign
git commits and/or other arbitrary blobs, you probably want to setup the below.

[different format]: https://github.com/openssh/openssh-portable/blob/master/PROTOCOL.u2f#L176-L187
[normal signing format]: https://github.com/openssh/openssh-portable/blob/master/PROTOCOL.sshsig#L79-L87

## `gpg-agent`

The other way to use SSH with a YubiKey requires more setup via `gpg`.  It has
the benefit of working exactly as one would expect if all SSH keys were on disk.

There is already a [good guide][] for this.  To fix an annoying [pin-entry
bug][] on Debian (and maybe other platforms), define the following aliases:

    alias ssh="gpg-connect-agent updatestartuptty /bye >/dev/null; ssh"
    alias ssh-keygen="gpg-connect-agent updatestartuptty /bye >/dev/null; ssh-keygen"

[good guide]: https://www.raymondcheng.net/posts/git-yubikey.html
[pin-entry bug]: https://github.com/drduh/YubiKey-Guide/issues/89#issuecomment-452474650
